<!DOCTYPE html>
<html lang="en">
<?php require_once("header.php"); ?>
<body>

    <!-- Navigation -->
    <?php require_once("navigation.php"); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

           <?php 
				require_once("search-form.php");	           
           require_once("category.php"); 
           ?>

            <div class="col-md-9">
						<!-- Göbek başlangıç !-->
						<?php require_once("load-data.php"); ?>
						<!-- Göbek Bitiş !-->
        

        <!-- Footer -->
       
       <?php require_once("footer.php"); ?>

     </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
