
<?php
include_once("inc/import.php");
include_once("inc/config.php");
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="<?php echo $Options_description; ?>">
    <meta name="author" content="Sami Babat" >
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/assets/jquery-1.11.3-jquery.min.js"></script>
<script src="js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="jquery.js"></script>


    <title><?php echo $Options_title; ?></title>

    <!-- Bootstrap Core CSS -->
	<link rel="icon" href="<?php echo $Options_favicon; ?>" type="image/png">
    <link href="css/bootstrap.css" rel="stylesheet">
	<!-- Purecss.io Css -->
	<link rel="stylesheet" href="css/pure-min.css">
		<link rel="stylesheet" href="css/custom.css">
    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>