<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Pisi Linux: BAKKAL - Yönetim</a>
          <div class="btn-group pull-right">
			<a class="btn" href="my-profile.html"><i class="icon-user"></i> $Admin</a>
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
			  <li><a href="my-profile.html">Profilim</a></li>
              <li class="divider"></li>
              <li><a href="#">Çıkış</a></li>
            </ul>
          </div>
          <div class="nav-collapse">
            <ul class="nav">
			<li><a href="index.html">Anasayfa</a></li>
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Yöneticiler <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="new-user.html">Yeni Yönetici</a></li>
					<li class="divider"></li>
					<li><a href="users.html">Yöneticileri Yönet</a></li>
				</ul>
			  </li>
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Yetkiler <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="new-role.html">Yeni Yetki</a></li>
					<li class="divider"></li>
					<li><a href="roles.html">Yetkileri Yönet</a></li>
				</ul>
			  </li>
			  <li><a href="stats.html">İstatistik</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    