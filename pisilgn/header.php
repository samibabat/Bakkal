<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once("include/conf.php");
    		 require_once("include/import.php");
         include('../class/SimpleImage/SimpleImage.php');
         include('../class/Self/self.php');
 ?>

    <meta charset="utf-8">
    <title>Pisi Linux: BAKKAL | Yönetim</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Sami Babat" >
    <link href="css/bootstrap.css" rel="stylesheet">
	  <link href="css/site.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo $local; ?>/lib/prism/prism.css">
    <script src="<?php echo $local; ?>/lib/prism/prism.js"></script>
    <script src='<?php echo $local; ?>/lib/tinymce/tinymce.min.js'></script>
  <script>
  tinymce.init({
  selector: 'textarea',
  height: 400,
  plugins: [
        "advlist searchreplace  autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste imagetools codesample "
    ],
    toolbar: "insertfile undo redo | styleselect  | searchreplace bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | fullscreen preview code codesample ",
    code_dialog_width: 1000

});
  </script>
    <script src="js/custom.js"></script>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>
