// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// Göster / Gizle

function toggle_password(target){
    var d = document;
    var tag = d.getElementById(target);
    var tag2 = d.getElementById("showhide");

    if (tag2.innerHTML == 'Şifreyi Göster'){
        tag.setAttribute('type', 'text');   
        tag2.innerHTML = 'Şifreyi Gizle';

    } else {
        tag.setAttribute('type', 'password');   
        tag2.innerHTML = 'Şifreyi Göster';
    }
}

function AddAChild () {
	var rand = Math.random().toString(30).slice(10)

     var newElem = document.createElement ("input");
     var newSpan = document.createElement ("span");
     newElem.style = "margin-left:160px; margin-top:20px;";
     newElem.name  = "CustomCode";
     newElem.id  = "code";
     newElem.value = rand;
	  var container = document.getElementById ("control-group");
     container.appendChild (newElem);

    }