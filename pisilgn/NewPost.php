
<?php require_once("header.php"); ?>
  <body>

     <?php require_once("navbar.php"); ?>
        <?php require_once("cat.php");
        ?>
    <div class="container-fluid">


        <div class="span9">
		  <div class="row-fluid">
			<div class="page-header">
				<h1>İçerik yönetimi <small>Yeni Ekle</small>
          <select  style="float:right;" onchange="javascript:handleSelect(this)">
          <option disabled selected> Varsayılan Dil: Türkçe</option>
          <option disabled> ----------------------</option>
          <option value="NewPost.php?Lang=TR"> Türkçe</option>
          <option value="NewPost.php?Lang=EN"> English </option>
        </select>
        <script type="text/javascript">
          function handleSelect(elm)
            {
              window.location = elm.value;
            }
          </script>
         </h1>
			</div>
      <?php
          if(@$_GET["NewPOST"]=="TRUE") {
           $Status 	= $_POST["durum"];
           $Title		= $_POST["baslik"];
           $Caption		= $_POST["caption"];
           $ImageURL	= $_POST["image"];
           $Url		= $_POST["url"];
           $Content		= $_POST["content"];
           $Date = date("d/m/Y");
           if($Status =="" || $Title =="" || $Caption =="" || $ImageURL =="" || $Content =="") { ?>
            <div class="alert alert-danger" role="alert"><b>Boş Alan bıraktın $user! Sağlam iş için tüm alanları doldur...</b></div>
          <?php } else {
                $NewPostQuery = "INSERT into post
                (baslik_TR,caption_TR,content_TR,image,url,status,date) VALUES
                ('$Title','$Caption','$Content','../upload/img/".sefyap($Title).".jpg','$Url','$Status','$Date')";
                if(mysql_query($NewPostQuery)) { ?>
            <div class="alert alert-success" role="alert"><b>Yeni İçerik eklendi</b></div>
            <?php
            $image = new SimpleImage();
            $image->load($ImageURL);
            $image->resize(750,270);
            $image->save('../upload/img/'.sefyap($Title).'.jpg');
            } else { ?>
              <div class="alert alert-danger" role="alert"><b>İçerik Eklenemedi!<br> Hata :<br> <?php echo  mysql_error(); ?></b></div>
          <?php  } } } ?>
      <form class="form-inline" method="POST" action="NewPost.php?NewPOST=TRUE">
				<fieldset>
          <div class="control-group">
            <label class="control-label" for="role"><b>DURUM</b></label>
            <div class="controls">
              <input name="durum" value="1" checked type="radio"><b>&nbsp;  Yayınla&nbsp;</b>
              <input name="durum" value="0" type="radio"> <b>&nbsp; Taslak olarak kaydet </b>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="role"><b>Başlık</b></label>
            <div class="controls">
              <input type="text" name="baslik" style="height:25px; width:100%;" class="input-xxlarge" id="role" />
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="role"><b>Kısa Açıklama</b></label>
            <div class="controls">
              <input type="text"  name="caption" style="height:50px; width:100%;" class="input-xxlarge" id="role" />

            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="role"><b>Resim</b></label>
            <div class="controls">
              <input type="text"  name="image" style="width:100%;" class="input-xxlarge" id="role" />
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="role"><b>URL</b></label>
            <div class="controls">
              <input type="text"  name="url" style=" width:100%;" placeholder="Eğer Dışarıya link vermiyorsan boş bırak..." class="input-xxlarge" id="role" />
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="role"><b>İçerik</b></label>
            <div class="controls">
                  <textarea  name="content" id="mytextarea" style=" width:100%; height:500px; ">Hello, World!</textarea>
              </div>
          </div>
						<input type="submit" class="btn btn-success btn-large" value="Kaydet" /> <a class="btn" href="admins.php">İptal</a>
				</fieldset>
			</form>

        </div>
      </div></div>

      <hr>

      <footer class="well">
        &copy; <a href="#">Pisi Linux</a> <div style="float:right"><a href="#"> Sami BABAT</a></div>

      </footer>

    </div>

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
  </body>
</html>
